from django.conf.urls import url, include
from django.views.decorators.csrf import ensure_csrf_cookie
from WebProject import settings
from WebApi.views import *
#from rest_framework.urlpatterns import format_suffix_patterns

media_urlpatterns=[
	url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes':True}),
]

users_urlpatterns= [
	url(r'^users/$', UserList.as_view()),    
    url(r'^users/(?P<pk>[0-9]+)/$', UserDetail.as_view(), name='user_detail'),
    url(r'^users/(?P<username>[\w.@+-]+)/$', user_detail_redirect),
    url(r'^users/(?P<pk>[0-9]+)/saved_places/$', SavedLocations.as_view(), name='user_saved_places'),
    url(r'^users/(?P<username>[\w.@+-]+)/saved_places/$', saved_locations_redirect),    
]

places_urlpatterns= [
	url(r'^places/$', LocationList.as_view()),      
    url(r'^places/(?P<pk>[0-9]+)/$', LocationDetail.as_view()),    
    url(r'^places/(?P<location_id>[0-9]+)/reviews/$', ReviewList.as_view()),
    url(r'^places/reports/$', ReportLocation.as_view()),
    url(r'^places/(?P<location_id>[0-9]+)/rating/$', RateLocation.as_view()),
]

tags_urlpatterns= [
	url(r'^tags/$', TagsList.as_view()),
]

authentication_urlpatterns = [    
    # url(r'^auth/', include('rest_framework_social_oauth2.urls')),
    url(r'^auth/login/$', Login.as_view()),
    url(r'^auth/logout/$', Logout.as_view()),
    url(r'^auth/register/$', RegisterUser.as_view()),
    url(r'^auth/password_change/$', PassWordChange.as_view()),    
    #python-social-auth urls
    url(r'^social_auth/login/$', SocialSignUpSignIn.as_view()), 
]

#urlpatterns = format_suffix_patterns(urlpatterns)
urlpatterns = media_urlpatterns + users_urlpatterns + places_urlpatterns + tags_urlpatterns \
    + authentication_urlpatterns
