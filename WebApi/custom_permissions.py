'''
Note: The instance-level has_object_permission method will only be called if the
view-level has_permission checks have already passed. 
Also note that in order for the instance-level checks to run, 
the view code should explicitly call .check_object_permissions(request, obj).
If you are using the generic views then this will be handled for you by default.
More Info at: http://www.django-rest-framework.org/api-guide/permissions/#custom-permissions
'''

from rest_framework import permissions


class IsSelfOrAdminOrAuthenticatedReadOnly(permissions.BasePermission):
	'''
	Request is allowed for authenicated users only.
	For unsafe requests, the user must be an admin or same as view's get_object()
	'''
	def has_permission(self, request, view):
		return request.user and request.user.is_authenticated()

	def has_object_permission(self, request, view, obj):		
		return (
			request.method in permissions.SAFE_METHODS or
			request.user == obj or
			request.user.is_staff
		)


class IsAdminOrReadOnly(permissions.BasePermission):
	def has_permission(self, request, view):
		return(
			request.method in permissions.SAFE_METHODS or 
			request.user and request.user.is_staff
		)


class IsAuthenticatedLocationPosterOrReadOnly(permissions.BasePermission):
	def has_permission(self, request, view):		
		return(
			request.method in permissions.SAFE_METHODS or
			(request.user and 
			request.user.is_authenticated() and
			request.user.id == int(request.data.get('poster')))				
		)


class IsLocationReporter(permissions.BasePermission):
	def has_permission(self, request, view):
		return (
			request.user and request.user.id == int(request.data.get('reporter'))
		)



class IsAuthenticatedOrCreate(permissions.IsAuthenticated):
	def has_permission(self, request, view):
		if request.method == 'POST':
			return True
		return super(IsAuthenticatedOrCreate, self).has_permission(request, view)