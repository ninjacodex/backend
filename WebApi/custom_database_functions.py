from django.db.models import Func, F


class Sin(Func):
	'''
	trigonometric sin() function
	'''
	function = 'SIN'


class Cos(Func):
	'''
	trigonometric cos() fucntion
	'''
	function = 'COS'


class Tan(Func):
	'''
	trigonometric tan() function
	'''
	function = 'TAN'


class ASin(Func):
	'''
	trigonometric asin() function
	'''
	function = 'ASIN'


class ACos(Func):
	'''
	trigonometric acos() function
	'''
	function = 'ACOS'


class ATan(Func):
	'''
	trigonometric atan() function
	'''
	function = 'ATAN'


class ToRad(Func):
	'''
	convert a degree angle to radians
	'''
	function = 'RADIANS'