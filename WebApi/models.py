from __future__ import unicode_literals
from django.db import models
from django.conf import settings
from easy_thumbnails.fields import ThumbnailerImageField
from rest_framework.authtoken.models import Token
from django.dispatch import receiver
from django.db.models.signals import post_save
import datetime
import uuid
import os
import copy


def random_photo_name(instance, filename):
    '''
    Generates a random unique name for the image file being uploaded.    
    Example, a file name (the entire file path) could be '<MEDIA_ROOT>/2016/03/img_opodame_e4d5f_10010243.png',
    meaning the file belongs to the user with the username, 'opodame' and was saved on the 10/Mar/2016 01:02:43.
    This is will be the saving/naming convention for the media files in the project.
    '''
    current_date = datetime.datetime.now()
    daytime_suffix = current_date.strftime('%d%H%M%S') #Example, 06121345, referring to 6th day, 12:13:45    
    file_extension = filename.split('.')[-1]    #Get the file extension of the uploaded file 
    main_directory_name = ''       
    if isinstance(instance,UserProfile):   
        instance_name = 'img_' + instance.user.username.replace(' ','')
        main_directory_name = 'Users'
    elif isinstance(instance, Location):
        instance_name = 'img_'+ instance.name.replace(' ','')
        main_directory_name = 'Locations_PrimaryPhotos'
    elif isinstance(instance, LocationSaves):
        instance_name = 'img_' + str(instance.location_id)
        main_directory_name = 'LocationSaves_MapAreas' #/LocationSavesMapAreas/
    else:
        instance_name = ''

    generated_file_name = '_'.join([instance_name, uuid.uuid4().hex[:5], daytime_suffix + '.' + file_extension])
    media_sub_directory = os.path.join(main_directory_name, current_date.strftime('%Y'), current_date.strftime('%m'))
    return os.path.join(media_sub_directory, generated_file_name)


def random_thumb_name(instance, filename):
    '''
    Generates a random unique name for the image file being uploaded.    
    Example, a file name (the entire file path) could be '<MEDIA_ROOT>/2016/03/img_opodame_e4d5f_10010243.png',
    meaning the file belongs to the user with the username, 'opodame' and was saved on the 10/Mar/2016 01:02:43.
    This is will be the saving/naming convention for the media files in the project.
    '''
    current_date = datetime.datetime.now()    
    daytime_suffix = current_date.strftime('%d%H%M%S') #Example, 06121345, referring to 6th day, 12:13:45    
    file_extension = filename.split('.')[-1]    #Get the file extension of the uploaded file        
    if isinstance(instance,UserProfile):   
        instance_name = instance.user.username.replace(' ','')     
    elif isinstance(instance, Location):
        instance_name = instance.name.replace(' ','')    
    instance_name ='thumb_' + instance_name
    generated_file_name = '_'.join([instance_name, uuid.uuid4().hex[:5], daytime_suffix + '.' + file_extension])
    media_sub_directory = os.path.join(type(instance).__name__ , current_date.strftime('%Y'), current_date.strftime('%m'))
    return os.path.join(media_sub_directory, generated_file_name)


class UserProfile(models.Model):
    user= models.OneToOneField(settings.AUTH_USER_MODEL, primary_key=True, related_name='profile')    
    photo = models.ImageField(upload_to=random_photo_name, blank=True, null=True)
    thumbnail = ThumbnailerImageField(
        upload_to=random_thumb_name, 
        resize_source=dict(autocrop=True, size=(50, 50), sharpen=True),
        blank=True, null=True, db_column='thumb_image')    
    last_modified=models.DateTimeField(auto_now=True)
    gcm_client_id = models.CharField(max_length=128, blank=True, null=True)    
    def __unicode__(self):   #__str__(self) in python 3
        return u'{}'.format(self.username)
    def save(self, *args, **kwargs):
        #delete photo file when updating profile's photo
        try:
            #if an object(old_self) is returned, then we are performing an update
            old_self = UserProfile.objects.get(user=self.user)         
        except: pass
        else:
            if old_self.photo != self.photo:    #if a new image is about to be saved
                old_self.photo.delete(save=False)
                old_self.thumbnail.delete(save=False)
            if not self.user.is_active:
                #delete photo thumbnail if the profile's user is deactivated
                self.thumbnail.delete()

        super(UserProfile, self).save(*args, **kwargs)

    #core fields in the AUTH_USER_MODEL
    #[username, password, email(optional), first_name(optional), last_name(optional), is_staff
    # is_active, is_superuser, last_login, date_joined, groups(m2m), user_permissions(m2m),]
    class Meta:
        db_table = 'auth_users_profiles'  #The corresponding table name in the database for this model

#Auto generated stuff for newly created auth users
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_user_profile(sender, instance, create=False, **kwargs): 
    '''
    create profile for new user
    '''   
    UserProfile.objects.get_or_create(user=instance)

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def generate_token(sender, instance, create=False, **kwargs):
    '''
    Generate token for new user
    '''
    Token.objects.get_or_create(user=instance)


class Location(models.Model):
    location_id = models.AutoField(primary_key=True)    #An auto generated id for the location
    name = models.CharField(max_length=45, db_column='location_name')
    description = models.CharField(max_length=255, blank=True, null=True)
    tags = models.ManyToManyField('Tag', through='LocationTag')  #The tags/categories of the location, 
    phone_number = models.CharField(max_length=25, blank=True, null=True)
    email = models.EmailField(max_length=254, blank=True, null=True)
    facebookID = models.CharField(max_length=50, blank=True, null =True)
    is_private = models.BooleanField(default=False) 
    is_estimated = models.BooleanField(default=False)    #determine if coordinates are exact
    longitude = models.DecimalField(max_digits=9, decimal_places=6)
    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    poster = models.ForeignKey(settings.AUTH_USER_MODEL, db_column='poster_id', on_delete=models.PROTECT)  #The user who posted the location on the platform
    datetime_posted = models.DateTimeField(auto_now_add=True)
    last_updated=models.DateTimeField(auto_now=True)    #The last time the location was editted.
    number_of_views = models.PositiveIntegerField(default=0, editable=False)   #The number of times viewed by users
    number_of_saves = models.PositiveIntegerField(default=0, editable=False) #the number if times saved
    is_active = models.BooleanField(default=True)   #indicates location is n
    datetime_deactivated = models.DateTimeField(blank=True, null=True)  #The date and time the location was deactivated, having been reported as 'bad' by a number of users    
    primary_photo_thumb = ThumbnailerImageField(  #the photo of the location when viewed in a long list of locations
        upload_to=random_thumb_name, 
        resize_source=dict(autocrop=True, size=(50, 50), sharpen=True),
        blank=True, null=True, db_column='thumbnail')
    # website = models.CharField(max_length=100)
    primary_photo = models.ImageField(upload_to=random_photo_name,blank=True,null=True)

    class Meta:
        db_table ='locations'#  table name in the database table for this model

    def __unicode__(self):   #__str__(self) in python 3
        '''    
        Human readable representation of an instance
        '''
        return u'{}'.format(self.name)    


class LocationShare(models.Model):
    location_share_id = models.AutoField(primary_key=True)  #Auto-generated id for the LOCATION SHARE
    location = models.ForeignKey('Location', on_delete=models.PROTECT, db_column='location_id')   #The LOCATION being shared
    sender = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, db_column='sender_id', related_name='locationshare') #The USER who shared the location
    recipient = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, db_column='recipient_id',related_name='locationreceived')  #The other USER with whom the LOCATION was shared
    short_message = models.CharField(max_length=50, blank=True)  #A short text sent with the share in the form of a message
    datetime_sent = models.DateTimeField(auto_now_add=True)   #The date and time the LOCATION was shared. Will be automatically set to the date and time the LOCATION SHARE was added to the database
    datetime_received = models.DateTimeField(blank=True, null=True) #The time the other USER(with whom the LOCATION is being shared) received the message.    

    class Meta:
        db_table = 'location_shares' #The name of the table in the database for this model


class Tag(models.Model):
    tag_id = models.AutoField(primary_key=True) #The auto-generated id for a tag
    name = models.CharField(unique=True, max_length=20, db_column='tag_name')   #The name of the tag, more like the display name of the tag itself
    # descendants = models.ManyToManyField('self', through='TagRelation',through_fields=('ancestor', 'descendant'), symmetrical=False)    
    
    # def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
    #     super(Tag, self).save(force_insert,force_update,using,update_fields)
    #     #if the tag is being inserted for the first time, it is immediately related to itself
    #     if force_insert:
    #         TagRelation.objects.create(ancestor=self,descendant=self)
    #     #the code below cannot be used. Since it will cause the compiler to be stuck in a loop
    #     #self.descendants.add(self)    #a tag is immediately related to its self        
    class Meta:
        db_table = 'tags'   #The corresponding table name in the database for this model


# class TagRelation(models.Model):
#     ancestor = models.ForeignKey('Tag', on_delete=models.PROTECT, related_name='tag_descendants')
#     descendant = models.ForeignKey('Tag', on_delete=models.PROTECT, related_name='tag_ancestors')
#     date_created = models.DateTimeField(auto_now_add=True)
#     class Meta:
#         db_table = 'tag_relations'
#         unique_together = (('ancestor','descendant'))


class Report(models.Model):
    location = models.ForeignKey('Location', on_delete=models.PROTECT, db_column='location_id') #The location on which a report is being made
    reporter = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, db_column='reporter_id')   #The user who made the report
    report = models.CharField(max_length=50)    #A short text detailing the report(what is wrong with the location)
    datetime_reported = models.DateTimeField(auto_now_add=True) #The date and time the report was made. Will be automatically set to the date and time the Report was added to the database
    def __unicode__(self):   #__str__(self) in python 3
        return u'({}, {})'.format(self.location.name, self.report)

    class Meta:
        db_table = 'reports'    #The corresponding table name in the database for this model
        unique_together = (('location', 'reporter'),)


class LocationTag(models.Model):
    location = models.ForeignKey('Location', db_column='location_id', on_delete = models.PROTECT) # the location to be tagged
    tag = models.ForeignKey('Tag', db_column = 'tag_id', on_delete = models.PROTECT) # tags of the location
    class Meta:
        db_table = 'location_tag' 
        unique_together = (('location', 'tag'))
    def __unicode__(self):
        return u'{}'.format(self.location, self.tag)        


class Rating(models.Model):
    location = models.ForeignKey(Location, db_column = 'location_id', related_name='rating') # the location to be rated
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete = models.PROTECT, db_column = 'user_id') # user that is doing the rating
    rating = models.PositiveSmallIntegerField() # the rating that was given
    datetime_rated = models.DateTimeField(auto_now_add = True) # the time the location was rated

    class Meta:
        db_table = 'ratings'    # name of the class as a column in the database
        unique_together = (('location', 'user'))

    def __unicode__(self):
        return u'{}'.format(self.location.name)        


class Review(models.Model):    
    review_id = models.AutoField(primary_key=True)   #Auto-generated id for the REVIEW
    location = models.ForeignKey(Location, on_delete=models.PROTECT, db_column='location_id', related_name='reviews')   #The LOCATION on which a REVIEW is being passed
    reviewer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, db_column='reviewer_id')  #The USER who passed the REVIEW on the Location
    review = models.CharField(max_length=255)  #A short text stating the REVIEW the USER passed
    datetime_reviewed = models.DateTimeField(auto_now_add=True)    #The date and time the REVIEW was passed. Will be automatically set to the date and time the REVIEW was passed
    last_modified = models.DateTimeField(auto_now=True) #The last time a user modified the REVIEW posted
    
    class Meta:        
        db_table = 'reviews'   #The table name in the database for this model


class LocationPhotos(models.Model):
    '''
    This model stores the photos of locations
    '''
    photo_id = models.AutoField(primary_key=True)   #Auto-generated id for the location photo
    photo = models.ImageField(upload_to=random_photo_name, db_column='photo_path')  #The photo of the location    
    location = models.ForeignKey(Location, on_delete=models.PROTECT, db_column='location_id', related_name = 'photos')
    #override and allow th deletion of the image from disk
    def delete(self, *args, **kwargs):
        self.photo.delete() #actually delete the image file from disk
        super(LocationPhotos,self).delete(*args, **kwargs)    

    class Meta:
        db_table = 'location_photos'    #The corresponding table name in the database for this model


class LocationViews(models.Model):
    '''
    This model stores the number of times a location has been uniquely viewed by users
    '''
    location_view_id = models.AutoField(primary_key=True)
    location = models.ForeignKey('Location', on_delete=models.PROTECT, db_column='location_id',related_name='location')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT,db_column='user_id',related_name='user')
    date_viewed = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table='location_views'
        unique_together = (('location','user'))


class LocationSaves(models.Model):
    location_save_id = models.AutoField(primary_key = True)
    location = models.ForeignKey('Location', on_delete=models.PROTECT, db_column='location_id')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, db_column='user_id', related_name='saved_locations')
    date_saved = models.DateTimeField(auto_now_add=True)
    map_image = models.ImageField(upload_to=random_photo_name, blank=True, null=True)

    class Meta:
        db_table='location_saves'
        unique_together = (('location', 'user'))
    