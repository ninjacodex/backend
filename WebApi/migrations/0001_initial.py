# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import easy_thumbnails.fields
import django.db.models.deletion
import WebApi.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Location',
            fields=[
                ('location_id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=45, db_column='location_name')),
                ('description', models.CharField(max_length=255, null=True, blank=True)),
                ('phone_number', models.CharField(max_length=25, null=True, blank=True)),
                ('email', models.EmailField(max_length=254, null=True, blank=True)),
                ('facebookID', models.CharField(max_length=50, null=True, blank=True)),
                ('is_private', models.BooleanField(default=False)),
                ('is_estimated', models.BooleanField(default=False)),
                ('longitude', models.DecimalField(max_digits=9, decimal_places=6)),
                ('latitude', models.DecimalField(max_digits=9, decimal_places=6)),
                ('datetime_posted', models.DateTimeField(auto_now_add=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('number_of_views', models.PositiveIntegerField(default=0, editable=False)),
                ('number_of_saves', models.PositiveIntegerField(default=0, editable=False)),
                ('is_active', models.BooleanField(default=True)),
                ('datetime_deactivated', models.DateTimeField(null=True, blank=True)),
                ('primary_photo_thumb', easy_thumbnails.fields.ThumbnailerImageField(null=True, upload_to=WebApi.models.random_thumb_name, db_column='thumbnail', blank=True)),
                ('primary_photo', models.ImageField(null=True, upload_to=WebApi.models.random_photo_name, blank=True)),
            ],
            options={
                'db_table': 'locations',
            },
        ),
        migrations.CreateModel(
            name='LocationPhotos',
            fields=[
                ('photo_id', models.AutoField(serialize=False, primary_key=True)),
                ('photo', models.ImageField(upload_to=WebApi.models.random_photo_name, db_column='photo_path')),
                ('location', models.ForeignKey(related_name='photos', db_column='location_id', on_delete=django.db.models.deletion.PROTECT, to='WebApi.Location')),
            ],
            options={
                'db_table': 'location_photos',
            },
        ),
        migrations.CreateModel(
            name='LocationSaves',
            fields=[
                ('location_save_id', models.AutoField(serialize=False, primary_key=True)),
                ('date_saved', models.DateTimeField(auto_now_add=True)),
                ('map_image', models.ImageField(null=True, upload_to=WebApi.models.random_photo_name, blank=True)),
                ('location', models.ForeignKey(db_column='location_id', on_delete=django.db.models.deletion.PROTECT, to='WebApi.Location')),
            ],
            options={
                'db_table': 'location_saves',
            },
        ),
        migrations.CreateModel(
            name='LocationShare',
            fields=[
                ('location_share_id', models.AutoField(serialize=False, primary_key=True)),
                ('short_message', models.CharField(max_length=50, blank=True)),
                ('datetime_sent', models.DateTimeField(auto_now_add=True)),
                ('datetime_received', models.DateTimeField(null=True, blank=True)),
                ('location', models.ForeignKey(db_column='location_id', on_delete=django.db.models.deletion.PROTECT, to='WebApi.Location')),
            ],
            options={
                'db_table': 'location_shares',
            },
        ),
        migrations.CreateModel(
            name='LocationTag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('location', models.ForeignKey(db_column='location_id', on_delete=django.db.models.deletion.PROTECT, to='WebApi.Location')),
            ],
            options={
                'db_table': 'location_tag',
            },
        ),
        migrations.CreateModel(
            name='LocationViews',
            fields=[
                ('location_view_id', models.AutoField(serialize=False, primary_key=True)),
                ('date_viewed', models.DateTimeField(auto_now_add=True)),
                ('location', models.ForeignKey(related_name='location', db_column='location_id', on_delete=django.db.models.deletion.PROTECT, to='WebApi.Location')),
            ],
            options={
                'db_table': 'location_views',
            },
        ),
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.PositiveSmallIntegerField()),
                ('datetime_rated', models.DateTimeField(auto_now_add=True)),
                ('location', models.ForeignKey(related_name='rating', db_column='location_id', to='WebApi.Location')),
            ],
            options={
                'db_table': 'ratings',
            },
        ),
        migrations.CreateModel(
            name='Report',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('report', models.CharField(max_length=50)),
                ('datetime_reported', models.DateTimeField(auto_now_add=True)),
                ('location', models.ForeignKey(db_column='location_id', on_delete=django.db.models.deletion.PROTECT, to='WebApi.Location')),
            ],
            options={
                'db_table': 'reports',
            },
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('review_id', models.AutoField(serialize=False, primary_key=True)),
                ('review', models.CharField(max_length=255)),
                ('datetime_reviewed', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('location', models.ForeignKey(related_name='reviews', db_column='location_id', on_delete=django.db.models.deletion.PROTECT, to='WebApi.Location')),
            ],
            options={
                'db_table': 'reviews',
            },
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('tag_id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=20, db_column='tag_name')),
            ],
            options={
                'db_table': 'tags',
            },
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('user', models.OneToOneField(related_name='profile', primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('photo', models.ImageField(null=True, upload_to=WebApi.models.random_photo_name, blank=True)),
                ('thumbnail', easy_thumbnails.fields.ThumbnailerImageField(null=True, upload_to=WebApi.models.random_thumb_name, db_column='thumb_image', blank=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('gcm_client_id', models.CharField(max_length=128, null=True, blank=True)),
            ],
            options={
                'db_table': 'auth_users_profiles',
            },
        ),
        migrations.AddField(
            model_name='review',
            name='reviewer',
            field=models.ForeignKey(db_column='reviewer_id', on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='report',
            name='reporter',
            field=models.ForeignKey(db_column='reporter_id', on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='rating',
            name='user',
            field=models.ForeignKey(db_column='user_id', on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='locationviews',
            name='user',
            field=models.ForeignKey(related_name='user', db_column='user_id', on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='locationtag',
            name='tag',
            field=models.ForeignKey(db_column='tag_id', on_delete=django.db.models.deletion.PROTECT, to='WebApi.Tag'),
        ),
        migrations.AddField(
            model_name='locationshare',
            name='recipient',
            field=models.ForeignKey(related_name='locationreceived', db_column='recipient_id', on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='locationshare',
            name='sender',
            field=models.ForeignKey(related_name='locationshare', db_column='sender_id', on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='locationsaves',
            name='user',
            field=models.ForeignKey(related_name='saved_locations', db_column='user_id', on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='location',
            name='poster',
            field=models.ForeignKey(db_column='poster_id', on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='location',
            name='tags',
            field=models.ManyToManyField(to='WebApi.Tag', through='WebApi.LocationTag'),
        ),
        migrations.AlterUniqueTogether(
            name='report',
            unique_together=set([('location', 'reporter')]),
        ),
        migrations.AlterUniqueTogether(
            name='rating',
            unique_together=set([('location', 'user')]),
        ),
        migrations.AlterUniqueTogether(
            name='locationviews',
            unique_together=set([('location', 'user')]),
        ),
        migrations.AlterUniqueTogether(
            name='locationtag',
            unique_together=set([('location', 'tag')]),
        ),
        migrations.AlterUniqueTogether(
            name='locationsaves',
            unique_together=set([('location', 'user')]),
        ),
    ]
