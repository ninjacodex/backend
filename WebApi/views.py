from django.shortcuts import redirect, get_object_or_404
from django.core.files import File
from django.db.models import Avg, Count, Q, F, Value, DecimalField, ExpressionWrapper
from django.db.models.functions import Coalesce
from django.contrib.auth import authenticate, login
from django.views.decorators.csrf import ensure_csrf_cookie, csrf_exempt
from django.template.context_processors import csrf
from django.http import HttpResponse
from rest_framework import generics, serializers, status
from rest_framework.views import APIView
from rest_framework.decorators import api_view, parser_classes
from rest_framework.response import Response
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.exceptions import *
from rest_framework.parsers import FormParser, MultiPartParser, JSONParser
from rest_framework.permissions import *
from .models import *
from .serializers import *
from .custom_database_functions import *
from .custom_permissions import *
from PIL import Image
from datetime import datetime
import re #for constructing regular expressions
import copy 	#for making copies of objects
# from social.apps.django_app import *
from social.apps.django_app.utils import load_backend, load_strategy
from social.backends.oauth import BaseOAuth1, BaseOAuth2
from social.exceptions import AuthAlreadyAssociated


denial_msg = "You do not have permission to perform this action"

class ExtraValidationMixin(object):
	'''
	Handles extra validation of requests
	'''
	parameters = []	#the query parameters that are permitted by the request
	valid_image_formats = ['.JPEG','.PNG','.JPG','.BMP','.GIF',]

	#check for invalid parameters in the request.query_params
	#invalid parameters are those that are not in self.parameters
	def validate_query_params(self):
		invalid_params = list(set(self.request.query_params).difference(set(self.parameters + ['format',])))
		if any(invalid_params):
			raise serializers.ValidationError({'Invalid query parameters':invalid_params})

	def validate_image(self, uploaded_photo):
		# check if the file's format is any of the acceptable ones		
		valid_image = uploaded_photo.name.upper().endswith(tuple(self.valid_image_formats))
		if not valid_image: raise serializers.ValidationError('Unacceptable file format. Use any of ' + str(self.image_formats))
		try:
			image = Image.open(File(copy.deepcopy(uploaded_photo)))	#attempt to open a copy of the image file
		except Exception, e:
			try:
				image.close()
				del image #to avoid memory wastage			
			except Exception: pass		
			raise serializers.ValidationError(e.message)
		image.close()
		del image

#Done
class UserList(generics.ListAPIView, ExtraValidationMixin):
	'''
	Search all users or create a new user object
	'''
	public_user_fields = ['id', 'username', 'first_name', 'last_name', 'email']  # the model fields to return in GET requests
	parser_classes = (MultiPartParser, FormParser,)
	queryset = User.objects.all().only(*public_user_fields).prefetch_related('profile')
	parameters = ['search']
	serializer_class = UserListSerializer
	#only authenicated users can view user info
	permission_classes = (IsAuthenticated,)

	def filter_queryset(self, queryset):
		self.validate_query_params()
		request = self.request		
		#select only users whose username or email or full_name contains the search_query if any
		if 'search' in request.query_params:
			search_query = request.query_params.get('search')	#get the search query from the request			
			username = Q(username__icontains = search_query)	#search in username field
			email = Q(email__icontains = search_query)	#search in email field
			first_name = Q(first_name__icontains = search_query)	#search in full_name field
			last_name = Q(last_name__icontains = search_query)
			queryset = queryset.filter(username | full_name | email | last_name)
		return queryset	

#Done
class RegisterUser(generics.CreateAPIView):
	'''
	Create a new user
	'''
	queryset = User.objects.all()
	parser_classes = (MultiPartParser, FormParser,)
	serializer_class = RegisterUserSerializer
	#anyone can register as a new user

	def create(self, request, *args, **kwargs):
		'''
		Creates a new user object based on the data of request and saves it
		'''	
		profile_photo = request.data.get('profile.photo')
		#use user's email as his/her username if username is not provided
		if 'username' not in request.data:
			try:				
				request.data.update({'username':request.data['email']})
			except KeyError:
				raise serializers.ValidationError("Either 'username' or 'email' must be provided")
		#if user's profile photo was added
		if profile_photo:
			try:
				#remove any existing instance of 'thumbnail' in the request
				#this is necessary as more than one entry of 'thumbnail' will
				#result in a tuple of values for 'thumbnail'
				request.data.pop('profile.thumbnail')
			except:	pass
			#attach thumbnial of user's photo
			request.data.update({'profile.thumbnail':copy.copy(profile_photo)})		
		response = super(RegisterUser, self).create(request, *args, **kwargs)
		user = 	User.objects.get(pk=response.data['id'])
		token = Token.objects.get(user=user)
		#return the user's auto-generated id and access token
		response.data = {'id':response.data['id'], 'user_access_token':token.key}
		return response

#Done
class UserDetail(generics.RetrieveUpdateDestroyAPIView, ExtraValidationMixin):
	parser_classes = (FormParser, MultiPartParser,)
	queryset = User.objects.filter(is_active=True)
	permission_classes = (IsAuthenticated,)	

	def get_serializer_class(self):
		if self.request.method == 'PUT' or self.request.method=='PATCH':
			return UserUpdateSerializer
		else:
			return UserDetailSerializer

	def destroy(self, request, *args, **kwargs):
		checkIsSelfOrAdmin(request.user, kwargs.get('pk'))
		instance = self.get_object()
		instance.is_active = False
		instance.profile.thumbnail.delete()
		instance.save()
		return Response(status = status.HTTP_204_NO_CONTENT)

	def update(self, request, *args, **kwargs):
		self.checkIsSelfOrAdmin(request.user, kwargs.get('pk'))				
		profile_photo = request.data.get('profile.photo')
		if profile_photo:	#if the user's profile photo was added									
				try:
					#remove any existing instance of 'thumbnail' in the request
					#this is necessary as more than one entry of 'thumbnail' will
					#result in a tuple of values for 'thumbnail'
					request.data.pop('profile.thumbnail')
				except:	pass
				#attach a thumbnail of the user's photo
				request.data.update({'profile.thumbnail':copy.copy(profile_photo)})		
		super(UserDetail, self).update(request,*args, **kwargs)
		return Response(status = status.HTTP_200_OK)

	def checkIsSelfOrAdmin(self, user, pk):
		if user.id != int(pk) and not user.is_staff:
			raise PermissionDenied(denial_msg)

@csrf_exempt
def user_detail_redirect(request, username):
	'''
	Use username to find a specific user
	'''
	user = get_object_or_404(User, username=username)
	return redirect('user_detail', pk=user.id)

#Done
class PassWordChange(generics.CreateAPIView):
	parser_classes = (JSONParser, FormParser, MultiPartParser,)
	permission_classes = (IsAuthenticated, )	#an admin should not be able to change the password of a user
	serializer_class = UserPasswordChangeSerializer

	def create(self, request, *args, **kwargs):
		#No user can change another user's password
		if request.user.username != request.data.get('username'):
			raise PermissionDenied(denial_msg)
		serializer = self.get_serializer(data=request.data)
		if serializer.is_valid(raise_exception=True):
			user = serializer.validated_data['user']	
			user.set_password(serializer.validated_data['new_password'])
			user.save()
			#refresh user's token
			try:
					current_token = Token.objects.get(user=user)
			except Token.DoesNotExist:	pass
			else:	current_token.delete()	#delete current_token if exists   	
			token = Token.objects.create(user=user)	#create a new random token		
		response = Response(data={'usertoken':token.key, 'message':'Password changed successfully',})	
		response.set_cookie('usertoken',token.key)	#TODO:Remove. For testing in postman only
		return response

#Done
class LocationList(generics.ListCreateAPIView, ExtraValidationMixin):
	#The poster of a location should be the same as the current authenticated user or admin
	permission_classes = (IsAuthenticatedOrReadOnly,)
	parser_classes = (FormParser, MultiPartParser,)
	parameters = ['search','deep','radius','latitude','longitude','tags']	

	def get_serializer_class(self):
		if self.request.method == 'GET':
			return LocationListSerializer
		else:
			return LocationEditSerializer

	def create(self, request, *args, **kwargs):
		'''
		create a new location object in the database
		'''
		#attach the poster of the location
		request.data.update({'poster':request.user.id})
		#get the photos of the location	
		photos = []
		prim_photo = None
		if 'photos' in request.data:
			photos = request.data.pop('photos')
			for photo in photos:	self.validate_image(photo)

		#choose and set the primary photo of the location
		if 'primary_photo' in request.data:
			prim_photo = request.data.get('primary_photo')				
			request.data.update({'primary_photo_thumb':copy.copy(prim_photo)}) 
		elif any(photos):	#use one of the photos as the primary photo
			request.data.update({'primary_photo':copy.copy(photos[0])})
			request.data.update({'primary_photo_thumb':copy.copy(photos[0])})

		#create the actual location instance(object)
		response = super(LocationList, self).create(request, *args, **kwargs)				

		#save the location's photos
		location_id = response.data['location_id']
		location_photos = [LocationPhotos(location_id = location_id,photo = photo) for photo in photos]
		LocationPhotos.objects.bulk_create(location_photos)
		
		#return the location id in a response
		response.data = {'location_id':location_id}
		# del photos
		# del location_id
		# del prim_photo
		return response

	def get_queryset(self):
		queryset = Location.objects.filter(is_active = True)
		return queryset

	def filter_queryset(self, queryset):
		self.validate_query_params()	#make sure all the query parameters are valid(acceptable)
		request = self.request
		search_query = request.query_params.get('search')
		deep_search = request.query_params.get('deep')
		radius = request.query_params.get('radius')
		latitude = request.query_params.get('latitude')
		longitude = request.query_params.get('longitude')
		tags = self.request.query_params.get('tags')	
		regex = re.compile('[\(\)\[\]\{\} ]')

		public_filter = Q(is_private = False)	#public locations only
		id = -999
		try:
			id = request.user.id
		except Exception: pass
		owner_filter = Q(poster_id = id)
		is_admin_filter = Q(poster__is_staff=True)
		is_shared_filter = Q()
		queryset = queryset.filter(public_filter | owner_filter | is_admin_filter)
		#apply distance filter and attach distance_ from_user
		#The 'distance' is calculated using Law of Cosines as opposed to the Haversine Formula				
		#distance(km) = acos(sin lat1 * sin lat2 + cos lat1 * cos lat2 * cos long1-long2) * EarthRadius			
		if latitude or longitude:
			try:				
				var = "'latitude'"
				latitude = float(latitude)				
				var = "'longitude'"
				longitude = float(longitude)				
			except:
				raise serializers.ValidationError('The ' + var + ' parameter should be a valid number')
			else:
				earth_radius = 6371.000
				queryset = queryset.annotate(
					distance_from_user = ExpressionWrapper(
						earth_radius * ACos((Sin(ToRad(F('latitude'))) * Sin(ToRad(latitude))) 
							+ (Cos(ToRad( F('latitude') )) * Cos(ToRad(latitude)) * Cos(ToRad(F('longitude')) - ToRad(longitude))) ), output_field = DecimalField()) )			
				if radius: #if the radius was provided, then filter the request based on the radius
					try:
						radius = float(radius)			
						queryset = queryset.filter(distance_from_user__lte=radius)
					except:					
							raise serializers.ValidationError("The 'radius' parameter should be a valid number")

		#apply filter based on search query
		if search_query:
			name_filter = Q(name__icontains = search_query)	#match name of location
			description_filter = Q(description__icontains = search_query)	#match description of location	
			if deep_search is not None:				
				if str(deep_search).upper() == 'TRUE':
					reviews_filter = Q(reviews__review__icontains = search_query) #match reviews on location
					queryset = queryset.filter(name_filter|description_filter|reviews_filter) #filter on name, description or reviews				
			else:
				queryset = queryset.filter(name_filter|description_filter)

		#apply filter based on the tags in queryparams
		if tags:
			tags = regex.sub('',str(tags))	#remove spaces and brackets in the tags parameter
			tags = tags.split(',') #split the parameter into idividual numbers.			
			try:
				tag_ids = [int(tag) for tag in tags]
			except ValueError:
				error = ("Format for 'tags' parameter is: tags = tag_1,tag_2,tag_3,... where each tag is a postive integer. For example, tags = [1,2,3,4,5,6]")
				raise serializers.ValidationError(error)	
			# # retrieve the descendants of the tags in the query parameters and use in search also	
			# temp_queryset = Tag.objects.all().filter(tag_id__in = tag_ids).prefetch_related('descendants')
			# #add the descendants of the tags to the list of tag ids
			# tag_ids = tag_ids + list(temp_queryset.values_list('descendants', flat = True))			
			# tag_ids = list(set(tag_ids))#remove duplicates from the set		
			queryset = queryset.filter(tags__in = tag_ids)
		#attach the location's average rating
		if self.request.method == 'GET':
			queryset = queryset.annotate(average_rating = Coalesce(Avg(F('rating__rating')),0.0))
		
		#free up memory
		del search_query
		del deep_search
		del radius
		del latitude
		del longitude
		del tags
		del regex

		return queryset	#return the final mutated queryset


class LocationDetail(generics.RetrieveUpdateDestroyAPIView, ExtraValidationMixin):
	parser_classes = (FormParser,MultiPartParser)
	parameters = ['delete_photos','delete_tags', 'delete_primary_photo', 'latitude', 'longitude']	
	queryset = Location.objects.filter(is_active = True)	
	permission_classes = (IsAuthenticatedOrReadOnly,)	

	def filter_queryset(self, queryset):
		self.validate_query_params()	#check for invalid query parameters		
		if self.request.method == 'GET':
			latitude = self.request.query_params.get('latitude')
			longitude = self.request.query_params.get('longitude')
			queryset = queryset.annotate(average_rating = Avg(F('rating__rating')))
			if latitude or longitude:
				try:				
					latitude = float(latitude)								
					longitude = float(longitude)				
				except:
					raise serializers.ValidationError('Both latitude and longitude parameters should be valid numbers')
				else:
					earth_radius = 6371.000
					queryset = queryset.annotate(
						distance_from_user = ExpressionWrapper(
							earth_radius * ACos((Sin(ToRad(F('latitude'))) * Sin(ToRad(latitude))) 
								+ (Cos(ToRad( F('latitude') )) * Cos(ToRad(latitude)) * Cos(ToRad(F('longitude')) - ToRad(longitude))) ), output_field = DecimalField()) )
		return super(LocationDetail, self).filter_queryset(queryset)

	def retrieve(self,request, *args, **kwargs):
		instance = self.get_object()		
		#if location is PRIVATE then the poster should be the current user or an admin,
		#or the location should have been shared with the user before	
		if instance.is_private and instance.poster_id != request.user.id and (
			not request.user.is_staff):
			#if the location has NEVER been shared with the current user
			if not LocationShares.objects.all().filter(
				Q(location_id=instance.location_id) & Q(recipient_id=request.user.id)).exists():
				raise PermissionDenied(denial_msg)
		#update the number of views of the location
		try:
			location_view = LocationViews.objects.filter(location_id=instance.location_id).get(
				id=request.user.id)
		except LocationViews.DoesNotExist:	#user has never viewed location before
			instance.number_of_views += 1
			instance.save()
		except AttributeError:	#user is an anonymous user
			pass
		return super(LocationDetail, self).retrieve(request,*args,**kwargs)

	def destroy(self, request, *args, **kwargs):
		instance = self.get_object()
		#only the poster of the location or an admin is allowed
		if instance.poster_id != request.user.id and not request.user.is_staff:
			raise PermissionDenied(denial_msg)
		instance.is_active = False	#set as inactive instead of deleting completely
		instance.date_deactivated = datetime.now()		
		instance.primary_photo_thumb.delete(save = False)	#delete location's thumbnail
		instance.save()
		return Response(status = status.HTTP_204_NO_CONTENT)
	
	def update(self, request, *args, **kwargs):
		if 'poster' in request.data:
			raise PermissionDenied("You cannot change the poster of a location.")
		photos = []
		instance = self.get_object()	#the location for which an update is being performed
		#only the poster of the location or an admin is allowed
		if instance.poster_id != request.user.id and not request.user.is_staff:
			raise PermissionDenied(denial_msg)		
		delete_primary_photo = ('delete_primary_photo' in request.query_params) and (
			request.query_params['delete_primary_photo'].upper() == 'TRUE')
		delete_tags = ('delete_tags' in request.query_params) and (
			request.query_params['delete_tags'].upper() == 'TRUE')
		delete_photos = ('delete_photos' in request.query_params) and (
			request.query_params['delete_photos'].upper() == 'TRUE')

		if delete_tags and not any(instance.tags):
			raise serializers.ValidationError('A location must have at least one tag')

		#retrieve the photos that were sent with the request
		if 'photos' in request.data:
			photos = request.data.pop('photos')
			for photo in photos:	self.validate_image(photo)	#check the validity of the incoming images		

		#delete attributes where necessary
		if delete_primary_photo:
			try:
				instance.primary_photo.delete(save=False)
				instance.primary_photo_thumb.delete()
			except Exception:	pass				
		if delete_tags:	#if the tags are to be deleted first, then we do so		
			instance.tags.all().delete()
		if delete_photos:	 #if the photos are to be deleted first, we do so
			for photo in instance.photos.all():
				photo.delete()			

		#A location must have a primary photo, once it has photos. 
		#One of the photos will be chosen as the primary photo if a primary photo is not given
		#but photos are given		
		if 'primary_photo' in request.data:
			request.data.update({'primary_photo_thumb':copy.copy(request.data.get('primary_photo'))})
		elif delete_primary_photo:
			if any(photos):
				#use the first of the newly given photos as the primary photo
				request.data.update({'primary_photo':copy.copy(photos[0])})
				request.data.update({'primary_photo_thumb':copy.copy(photos[0])})
			elif any(instance.photos):
				#use any of the existing photos as the primary photo
				request.data.update({'primary_photo':copy.copy(instance.photos[0])})
				request.data.update({'primary_photo_thumb':copy.copy(instance.photos[0])})			

		#get and return the final response
		response = super(LocationDetail, self).update(request, *args, **kwargs)	
		location_photos = [LocationPhotos(location = instance,photo = photo) for photo in photos]
		LocationPhotos.objects.bulk_create(location_photos)
		del photos 	#free some memory
		response.data = None #clear the response's data in order to reduce data/bandwidth usage				
		return response

	def get_serializer_class(self, **kwargs):
		if self.request.method == 'GET':
			return LocationDetailSerializer
		elif self.request.method == 'PUT' or self.request.method == 'PATCH':
			return LocationEditSerializer


class TagsList(generics.ListCreateAPIView, ExtraValidationMixin):
	parser_classes = (FormParser, MultiPartParser,)
	queryset = Tag.objects.all()
	serializer_class = TagSerializer
	#anyone can read list of tags. Only an admin can create a new tag
	permission_classes = (IsAdminOrReadOnly,)

	def filter_queryset(self, queryset):
		self.validate_query_params()
		return super(TagsList, self).filter_queryset(queryset)

	# def get_serializer_class(self):
	# 	if self.request.method == 'GET':
	# 		return TagSerializer
	# 	elif self.request.method == 'POST':
	# 		return SerializerHelper.LocationTagSerializer

	def create(self, request, *args, **kwargs):
		response = super(TagsList, self).create(request, *args, **kwargs)
		response.data = {'tag_id':response.data['tag_id']}
		return response


class ReportLocation(generics.CreateAPIView):
	parser_classes = (FormParser, MultiPartParser,)
	queryset = Report.objects.all()
	serializer_class = ReportSerializer
	#only authenticated users can report a location
	permission_classes = (IsAuthenticated,)


class ReviewList(generics.ListCreateAPIView):
	parser_classes = (FormParser,MultiPartParser,)
	queryset = Review.objects.filter(location__is_active = True)
	serializer_class = ReviewSerializer
	lookup_field = 'location_id'
	#anyone can read the review. Only authenticated users can write reviews
	permission_classes = (IsAuthenticatedOrReadOnly,)

	def create(self, request, *args, **kwargs):
		location_id = kwargs.get('location_id')
		if not Location.objects.filter(Q(location_id=location_id) & Q(is_active=True)).exists():
			raise NotFound() #check if the location exists and is active
		serializer = self.get_serializer(data = request.data)
		serializer.is_valid(raise_exception = True)
		#check if reviewer is the same as the current user
		if request.user.id != serializer.validated_data['reviewer']:
			raise PermissionDenied('You do not have permission to post on behalf on another user')
		validated_data = dict({'location_id':location_id})
		validated_data.update(serializer.validated_data)
		review = Review.objects.create(**validated_data)
		return Response(status = status.HTTP_201_CREATED)	


class RateLocation(generics.ListCreateAPIView):
	parser_classes = (FormParser, MultiPartParser,)
	queryset = Rating.objects.all()	
	permission_classes = (IsAuthenticatedOrReadOnly,)

	def get_serializer_class(self):
		if self.request.method == 'POST':
			return RatingSerializer
		else:
			return RatingDetailSerializer

	def create(self, request, *args, **kwargs):		
		location_id = kwargs.get('location_id')
		serializer = self.get_serializer(data = request.data)
		serializer.is_valid(raise_exception = True)
		#check if the current user is the same as the 'rator' of the rating
		if request.user.id != serializer.validated_data['user']:
			raise PermissionDenied(denial_msg)
		try:
			location = Location.objects.filter(is_active = True).get(location_id = location_id)
		except Location.DoesNotExist, e:
			raise NotFound(detail = e.message)
		validated_data = dict({'location':location})
		#append the validated_data dictionary to the serializer's validated data
		validated_data.update(serializer.validated_data)
		rating = Rating.objects.create(**validated_data)
		return Response(status = status.HTTP_201_CREATED)	
	
	def list(self, request, *args, **kwargs):
		location_id = kwargs.get('location_id')
		total_rating_count = self.queryset.filter(location_id = location_id).count()
		queryset = Rating.objects.filter(location_id = location_id).values('rating').annotate(count = Count(F('id'))).order_by('-rating')		
		serializer = self.get_serializer(queryset, many=True)
		return Response(serializer.data, status = status.HTTP_200_OK)


class SavedLocations(generics.ListCreateAPIView, generics.DestroyAPIView, ExtraValidationMixin):
	parser_classes = (FormParser, MultiPartParser,)
	id = None
	parameters = ['location_id']
	queryset = LocationSaves.objects.all()
	permission_classes = (IsAuthenticated,)	

	def get_serializer_class(self):
		if self.request.method == 'GET':
			return LocationSaveListSerializer
		else:
			return LocationSaveEditSerializer	
	
	def filter_queryset(self, queryset):		
		return queryset.filter(user_id=self.id)

	def create(self, request, *args, **kwargs):
		location_id = request.data.get('location')
		if not Location.objects.filter(Q(location_id=location_id) & Q(is_active=True)).exists():
			return Response(data={'details':'No location found'}, status=status.HTTP_400_BAD_REQUEST)		
		self.id = kwargs.get('id')	#the id of the user for which a location is being saved		
		request.data.update({'user':self.id})	#add id to the request		
		if request.user.id != request.data.get('user') and not request.user.is_staff:
			raise PermissionDenied(denial_msg)
		response = super(SavedLocations, self).create(request, *args, **kwargs)
		location.number_of_saves += 1
		response.data = None
		return response

	def list(self, request, *args, **kwargs):
		self.id = kwargs.get('id')
		#Saved places can only be accessed by the owner or an admin
		if request.user.id != self.id and not request.user.is_staff:
			raise PermissionDenied(denial_msg)		
		return super(SavedLocations, self).list(request, *args, **kwargs)	

	def destroy(self, request, *args, **kwargs):
		self.id = kwargs.get('id')
		#Saved places can only be accessed by the owner or an admin
		if request.user.id != self.id and not request.user.is_staff:
			raise PermissionDenied(denial_msg)	
		self.validate_query_params()	#check for invalid query parameters
		location_id = request.query_params.get('location_id')
		if not location_id:
			raise serializers.ValidationError('Provide the location id')		
		try:
			saved_location = LocationSaves.objects.filter(user_id=id).get(location_id=location_id)			
		except LocationSaves.DoesNotExist:
			return Response(status=status.HTTP_204_NO_CONTENT)
		saved_location.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)

@csrf_exempt
def saved_locations_redirect(request, username):
	user = get_object_or_404(User, username=username)
	return redirect('user_saved_places',id=user.id)

#Done
class Login(ObtainAuthToken):
	'''
	Gets the token for a user, but with additional token refresh support
	'''	
	#anyone is allowed to attempt to login
	serializer_class = DualAuthTokenSerializer

	def post(self, request, *args, **kwargs):
		if 'username' not in request.data:
			try:				
				request.data.update({'username':request.data['email']})
			except KeyError:
				raise serializers.ValidationError("Either of 'username' or 'email' must be provided")
		serializer = self.serializer_class(data=request.data)
		serializer.is_valid(raise_exception=True)		
		user = serializer.validated_data['user']		
		try:
			#delete old user_token if any
			current_token = Token.objects.get(user=user)
			current_token.delete()
		except Token.DoesNotExist:	pass
		token = Token.objects.create(user=user)	#create a new random token
		user_data = dict(UserDetailSerializer(user).data)
		user_data.update({'user_access_token': token.key,})
		response = Response(user_data)
		return response


class SocialSignUpSignIn(APIView):
	permission_classes = (IsAuthenticatedOrCreate, )
	serializer_class = SocialSignUpSerializer
	parser_classes = (FormParser, MultiPartParser, )
	def post(self, request, *args, **kwargs):
		serializer = self.serializer_class(data=request.data)
		serializer.is_valid(raise_exception=True)
		provider = serializer.validated_data['provider']
		#try to associate the social account with the current user if present
		authed_user = None if request.user.is_anonymous() else request.user
		# `strategy` is a PSA concept that denotes the exact framework to
		# be used (Django, Flask, or whatever).
		#'Django' will be used if request is passed to load_strategy()
		strategy = load_strategy(request)
		# Get the backend that corresponds to the social auth provider
		# e.g., Facebook, Google-Oauth2, etc.
		backend = load_backend(strategy=strategy, name=provider, redirect_uri=None)
		token = None
		if isinstance(backend, BaseOAuth1):
			#OAuth1 backends require that you pass 'oauth_token_secret as well.'
			#Eg: Twitter
			token = {
				'oauth_token': serializer.validated_data['access_token'],
				'oauth_token_secret': serializer.validated_data['access_token_secret'],
			}
		elif isinstance(backend, BaseOAuth2):
			# We're using oauth's implicit grant type (usually used for web and mobile 
			# applications), so all we have to pass here is an access_token
			token = request.data['access_token']
		try:
			#associate the user with the social account or create a new one
			user = backend.do_auth(token, user=authed_user)
		except AuthAlreadyAssociated:
			#A social account can't be associated with more than one user
			return Response({"errors": "That social media account is already in use"}, 
				status=status.HTTP_400_BAD_REQUEST)
		except:
			return Response({'errors':'An error occurred while authenticating the user'}, 
				status=status.HTTP_400_BAD_REQUEST)
		if user and user.is_active:
			# if the access token was set to an empty string, then save the access token 
			# from the request
			auth_created = user.social_auth.get(provider=provider)
			if not auth_created.extra_data['access_token']:
				# Facebook for example will return the access_token in its response to you. 
				# This access_token is then saved for your future use. However, others 
				# e.g., Instagram do not respond with the access_token that you just 
				# provided. We save it here so it can be used to make subsequent calls.
				auth_created.extra_data['access_token'] = token
				auth_created.save()
			try:
				#delete old user_token if any
				current_token = Token.objects.get(user=user)
				current_token.delete()
			except Token.DoesNotExist:	pass
			token = Token.objects.create(user=user)	#create a new random token
			user_data = dict(UserDetailSerializer(user).data)
			user_data.update({'user_access_token': token.key,})
			response = Response(user_data, status=status.HTTP_201_CREATED)
			return response			
		else:
			return Response({"errors": "Error with social authentication"},
							status=status.HTTP_400_BAD_REQUEST)

#Done
class Logout(APIView):
	permission_classes = (IsAuthenticated,)
	def post(self, request, *args, **kwargs):		
		try:
			token = Token.objects.get(user=request.user)
			token.delete()				
		except:
			pass			
		return Response()