from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework import serializers
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.conf import settings
from WebProject import settings as proj_settings
from .models import *
import re


password_regex = re.compile(r'^(?=.*?\d)(?=.*?[a-zA-Z])[A-Za-z\d]{8,}$')




class SerializerHelper:
	class LocationPosterSerializer(serializers.ModelSerializer):
		'''
		Serializer for the posters of locations. Only the poster's id and username are serialized
		'''
		class Meta:
			model = User
			fields = ('id','username',)	
			read_only_fields = ('id','username',)	#pass only the id of the user, when posting a new location		

	class LocationPhotoSerializer(serializers.ModelSerializer):
		photo = serializers.ImageField()
		class Meta:
			model=LocationPhotos
			fields=('photo',)



class LocationListSerializer(serializers.ModelSerializer):
	tags = serializers.SlugRelatedField(many=True, queryset=Tag.objects.all(), slug_field='name')
	poster = serializers.SlugRelatedField(queryset=User.objects.all(), slug_field='username')



class TagSerializer(serializers.ModelSerializer):
	class Meta:
		model = Tag
		fields = ('tag_id', 'name', )
		read_only_fields = ('tag_id',)


class LocationListSerializer(serializers.ModelSerializer):	
	tags = serializers.SlugRelatedField(many=True, queryset=Tag.objects.all(), slug_field='name')		
	average_rating = serializers.DecimalField(max_digits=2, decimal_places=1, required=False)
	poster = serializers.SlugRelatedField(queryset= User.objects.all(), slug_field='username')
	distance_from_user = serializers.DecimalField(max_digits=10, decimal_places=2, required=False)	

	class Meta:
		model = Location
		fields = ('location_id','name','description','phone_number','longitude','latitude','is_private',
			'is_estimated','poster','datetime_posted','last_updated','number_of_views','tags','primary_photo_thumb',
			'number_of_saves', 'average_rating', 'distance_from_user')
		read_only_fields = ('average_rating', 'distance_from_user')	



class LocationEditSerializer(serializers.ModelSerializer):
	tags = serializers.SlugRelatedField(many=True, queryset=Tag.objects.all(), slug_field='name')
	# poster = serializers.SlugRelatedField(queryset=User.objects.all(), slug_field='username')
	photos = SerializerHelper.LocationPhotoSerializer(many=True, read_only=False)

	class Meta:
		model = Location
		fields = ('location_id','name','description','phone_number','longitude','latitude','is_private',
			'is_estimated','poster','tags','primary_photo_thumb','primary_photo', 'photos',)
			
	def create(self, validated_data):
		#remove photos from the validated data, else an exception will be thrown
		#Location.objects.create(**kwargs) cannot take 'photos' as an argument
		if 'photos'	in validated_data:
			validated_data.pop('photos')
		#get the tags of the location
		try:
			tags = validated_data.pop('tags')
			if len(list(tags))==0:
				raise Exception
			tag_objects = Tag.objects.filter(name__in=list(tags))
		except Exception, e:
			raise serializers.ValidationError('Location must have at lease one(1) tag')
		#create the location object
		location = Location.objects.create(**validated_data)
		#create the tags of the location
		if list(tag_objects) != 0:			
			location_tags_objects = [LocationTag(location=location, tag=tag_object) for tag_object in tag_objects]
			LocationTag.objects.bulk_create(location_tags_objects)
		else:
			raise serializers.ValidationError('Location must have at lease one(1) tag')			
		return location

	def update(self, instance, validated_data):
		#NB: Photos are updated in the corresponding view in views.py
		instance.name = validated_data.get('name',instance.name)
		instance.description = validated_data.get('description', instance.description)
		instance.phone_number = validated_data.get('phone_number', instance.phone_number)
		instance.longitude = validated_data.get('longitude',instance.longitude)
		instance.latitude = validated_data.get('latitude',instance.latitude)
		instance.is_private = validated_data.get('is_private', instance.is_private)
		instance.is_estimated = validated_data.get('is_estimated',instance.is_estimated)
		instance.primary_photo = validated_data.get('primary_photo',instance.primary_photo)		
		instance.primary_photo_thumb = validated_data.get('primary_photo_thumb', instance.primary_photo_thumb)
		instance.save()
		
		#Update the list of tags of the location
		if ('tags' in validated_data):
			tag_names = [tag.name for tag in validated_data.get('tags')]
			existing_tag_names = [tag.name for tag in instance.tags.all()]	#the current tags of the location

			new_tag_names = list(set(tag_names)-set(existing_tag_names))	#the tags to be saved	
			tags = Tag.objects.filter(name__in=new_tag_names)
			location_tags = [LocationTag(location=instance,tag=tag) for tag in list(tags)]	
			try:
				LocationTag.objects.bulk_create(location_tags)
			except Exception, e:
				raise serializers.ValidationError(e.message)
		return instance


class LocationDetailSerializer(serializers.ModelSerializer):
	'''
	Serializer for the detial view of a location
	'''	
	poster = SerializerHelper.LocationPosterSerializer()
	tags = TagSerializer(many=True)
	photos = SerializerHelper.LocationPhotoSerializer(many=True)
	average_rating = serializers.DecimalField(max_digits=2, decimal_places=1, required=False)
	distance_from_user = serializers.DecimalField(max_digits=10, decimal_places=2, required=False)
	class Meta:
		model = Location
		fields = ('location_id','name','description','phone_number','longitude','latitude','is_private',
			'is_estimated','poster','datetime_posted','last_updated','number_of_views','number_of_saves', 'tags','photos',
			'primary_photo','average_rating','distance_from_user',)
		read_only_fields = ('average_rating', 'distance_from_user')
	# def to_representation(self, instance):
	# 	data = super(LocationDetailSerializer, self).to_representation(instance)
	# 	if hasattr(instance,'average_rating'):	#incoming objets do not have this attribute
	# 		data.update({'rating':instance.average_rating})
	# 	return data 


class UserListSerializer(serializers.ModelSerializer):	
	class UserProfileListSerializer(serializers.ModelSerializer):
		class Meta:
			model = UserProfile			
			fields = ('thumbnail',)	
	profile = UserProfileListSerializer()
	class Meta:
		model = User
		fields = ('id','username', 'email', 'first_name', 'last_name', 'profile',)		


class UserDetailSerializer(serializers.ModelSerializer):
	class UserProfileSerializer(serializers.ModelSerializer):
		class Meta:
			model = UserProfile
			fields = ('photo',)	

	profile = UserProfileSerializer()
	# photo = serializers.SerializerMe

	class Meta:
		model = User
		fields = ('id','username', 'email', 'first_name', 'last_name', 'profile',)		


class UserUpdateSerializer(UserDetailSerializer):
	'''
	Serializer for updating user. Cannot change user's password with this
	'''
	class Meta(UserDetailSerializer.Meta):
		fields = ('username', 'email', 'first_name', 'last_name', 'profile',)

	def update(self, instance, validated_data):
		instance = instance
		profile_data = None
		if 'profile' in validated_data:			
			profile_data = validated_data.pop('profile')
		#update auth_user info		
		for attr, value in validated_data.items():
			setattr(instance, attr, value)				
		instance.save()
		if profile_data:
			#update user' profile info
			for (attr, value) in profile_data.items():
				setattr(instance.profile, attr, value)			
			instance.profile.save()

		return instance


class RegisterUserSerializer(UserDetailSerializer):

	class UserProfileSerializer(serializers.ModelSerializer):
		class Meta:
			model = UserProfile
			fields = ('photo','thumbnail','gcm_client_id')
			#thumbnail will be automatically generated from photo
			extra_kwargs = {'thumbnail':{'write_only':True}, 'gcm_client_id':{'write_only':True},}

	profile = UserProfileSerializer(read_only=False, required=False)
	email = serializers.EmailField(required=True)

	class Meta:
		model = User
		fields = ('id','username', 'password', 'email', 'first_name', 'last_name', 'profile',)
		read_only_fields = ('id')
		extra_kwargs = {'password':{'write_only':True},}

	def validate_password(self, value):
		if not password_regex.match(value):
			raise serializers.ValidationError("Password must be 8 or more alphanumeric characters")
		return value

	#NOTE: UserProfile objects are created for every user automatically by listening to the 
	#django.db.models.signals.post_save signal of auth_user and creating related UserProfile objects
	#this is done when User.objects.create_user() is called
	def create(self, validated_data):
		profile_data = None
		if 'profile' in validated_data:			
			profile_data = validated_data.pop('profile')
		user = User.objects.create_user(**validated_data)				
		if profile_data:	#if profile data was added to the request
			for (attr, value) in profile_data.items():
				setattr(user.profile, attr, value)
			user.profile.save()
		return user	


class UserPasswordChangeSerializer(serializers.ModelSerializer):
	new_password = serializers.CharField()
	password = serializers.CharField()
	username = serializers.CharField()

	class Meta:
		model = User
		fields = ('username', 'password', 'new_password',)

	def validate_new_password(self, value):	
		if not password_regex.match(value):
			raise serializers.ValidationError("Password must be 8 or more alphanumeric characters")
		return value

	def validate(self, attrs):
		username = attrs.get('username')
		password = attrs.get('password')
		if username and password:
			user = dual_authenticate(username=username, password=password)
			if user:
				if not user.is_active:                    
					raise serializers.ValidationError('User account is disabled.')
			else:           
				raise serializers.ValidationError('Unable to log in with provided credentials.')
		else:            
			raise serializers.ValidationError('Must include "username" and "password".')
		attrs['user'] = user
		return attrs


class DualAuthTokenSerializer(AuthTokenSerializer):

	def validate(self, attrs):
		username = attrs.get('username')
		password = attrs.get('password')
		if username and password:
			#authenticate with username/email and password
			user = dual_authenticate(username=username, password=password)
			if user:
				if not user.is_active:
					raise serializers.ValidationError('User account is disabled.')
			else:
				raise serializers.ValidationError('Unable to log in with provided credentials.')
		else:            
			raise serializers.ValidationError('Must include "username" and "password".')
		attrs['user'] = user
		return attrs


class SocialSignUpSerializer(serializers.Serializer):
	provider = serializers.CharField()
	access_token = serializers.CharField()
	access_token_secret = serializers.CharField(required=False)
	
	def validate_provider(self, value):
		if not value: raise serializers.ValidationError('provider cannot be null')
		if value not in proj_settings.PLACELOCATOR_SETTINGS['ACCEPTED_SOCIAL_AUTH_PROVIDERS']: 
			raise serializers.ValidationError('unsupported provider, {}'.format(value))
		return value

class ReportSerializer(serializers.ModelSerializer):
	'''
	This serializer is for 'bad' reports on locations
	'''
	class Meta:
		model = Report
		fields=('location','reporter','report',)


class ReviewSerializer(serializers.ModelSerializer):
	'''
	Serializer for comments about locations
	'''	
	class Meta:
		model = Review
		fields=('location', 'reviewer','review', 'datetime_reviewed',)
		#location field is read only because it will be passed as an argument in the url
		read_only_fields = ('location','datetime_reviewed',)


class RatingSerializer(serializers.ModelSerializer):
	rating = serializers.IntegerField(min_value=1,max_value=5)
	class Meta:
		model = Rating
		fields= ('location','user','rating',)
		#location field is read only because it will be passed as a query argument in the url
		read_only_fields = ('location',)


class RatingDetailSerializer(serializers.ModelSerializer):
	rating = serializers.IntegerField()
	count = serializers.IntegerField()	
	class Meta:
		model = Rating
		fields = ('rating','count')


class LocationSaveListSerializer(serializers.ModelSerializer):
	'''
	Serializer used to retrieve users' saved locations
	'''
	location = LocationDetailSerializer()
	class Meta:
		model = LocationSaves
		fields = ('location_save_id', 'location', 'map_image', 'user', 'date_saved')
		read_only_fields = ('location', 'date_saved', 'user')


class LocationSaveEditSerializer(serializers.ModelSerializer):
	'''
	Serializer used to create a saved location for a user 
	'''
	class Meta:
		model = LocationSaves
		fields = ('location', 'map_image', 'user',)		

def dual_authenticate(username=None, password=None):
		'''
		authenticate user with username/email and password
		based on django.contrib.auth.authenticate
		'''
		user = None
		try:
			#incase the username passed is the email
			user = User.objects.get(email=username)
			user = authenticate(username=user.username, password=password)		
			if not user:
				raise User.DoesNotExist
		except User.DoesNotExist:
			user = authenticate(username=username, password=password)
		return user
